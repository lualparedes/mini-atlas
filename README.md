# Mini Atlas

A cool pet project displaying a mini atlas of the world.

## Stack

- React
- Apollo Client (GraphQL)
- Docker
