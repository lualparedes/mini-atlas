import { createContext } from "react";

export const DEFAULT_COUNTRY = "";

export type TCountryContext = [string, (countryId: string) => void];

const CountryContext = createContext([DEFAULT_COUNTRY, new Function()]);

export default CountryContext;
