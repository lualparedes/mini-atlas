import React from "react";
import { render, screen } from "@testing-library/react";

import { LOGO_TITLE } from "components/Logo";

import App from "./App";

test("renders header", () => {
  render(<App />);
  const headerLogo = screen.getByTitle(LOGO_TITLE);
  expect(headerLogo).toBeInTheDocument();
});
