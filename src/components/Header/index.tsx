import React from "react";

import Logo from "components/Logo";

import styles from "./style.module.css";

const Header = () => {
  return (
    <div className={styles.header}>
      <Logo className={styles.logo} />
    </div>
  );
};

export default Header;
