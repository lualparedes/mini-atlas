import React from "react";
import { render } from "@testing-library/react";

import { LOGO_TITLE } from "components/Logo";

import Header from "./index";

describe("Header", () => {
  it("renders logo", () => {
    const { getByTitle } = render(<Header />);
    expect(getByTitle(LOGO_TITLE)).toBeInTheDocument();
  });
});
