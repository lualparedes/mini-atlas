import React from "react";

import Card from "components/Card";
import MapContainer from "components/MapContainer";

import styles from "./style.module.css";

const Main = (): JSX.Element => {
  return (
    <main className={styles.main}>
      <MapContainer className={styles.mainItem} />
      <Card className={styles.mainItem} />
    </main>
  );
};

export default Main;
