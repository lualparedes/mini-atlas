export interface ILanguage {
  name: string;
}

export const formatIntoTextList = (
  parts: string[],
  connector: string
): string => {
  const formattedParts = [];
  const useCommas = parts.length > 2;

  for (let i = 0; i < parts.length - 1; i++) {
    formattedParts.push(`${parts[i]}${useCommas ? "," : ""}`);
  }
  formattedParts.push(`${connector} ${parts.at(-1)}`);

  return formattedParts.join(" ");
};

export const getPhoneCodesFrom = (phoneCodes: string): string => {
  if (phoneCodes.includes(",")) {
    return `codes ${formatIntoTextList(
      phoneCodes.split(",").map((c) => `+${c}`),
      "or"
    )}`;
  }
  return `code +${phoneCodes}`;
};

export const getCurrencyCodesFrom = (currency: string): string => {
  if (currency.includes(",")) {
    return `codes ${formatIntoTextList(currency.split(","), "or")}`;
  }
  return `code ${currency}`;
};

export const getLanguagesFrom = (languages: ILanguage[]): string => {
  if (languages.length > 1) {
    return formatIntoTextList(
      languages.map((l) => l.name),
      "and"
    );
  }

  return languages[0].name;
};
