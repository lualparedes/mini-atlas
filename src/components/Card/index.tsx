import React, { useContext } from "react";

import { gql, useQuery } from "@apollo/client";
import CloseIcon from "@mui/icons-material/Close";
import clsx from "clsx";

import CountryContext, { DEFAULT_COUNTRY, TCountryContext } from "context";

import styles from "./style.module.css";
import {
  getCurrencyCodesFrom,
  getLanguagesFrom,
  getPhoneCodesFrom,
} from "./utils";

const InitialCardContent = (): JSX.Element => {
  return (
    <>
      <h1 className={styles.title}>Discover the world...</h1>
      <p className={styles.text}>
        Select a country to see information about it. Pan and zoom around the
        world if needed.
      </p>
    </>
  );
};

interface ICountryCardContentProps {
  close: VoidFunction;
  countryId: string;
}

const GET_COUNTRY = gql`
  query Country($id: ID!) {
    country(code: $id) {
      name
      capital
      continent {
        name
      }
      currency
      phone
      languages {
        name
      }
      emoji
    }
  }
`;

const CountryCardContent = ({
  close,
  countryId,
}: ICountryCardContentProps): JSX.Element => {
  const { loading, error, data } = useQuery(GET_COUNTRY, {
    variables: { id: countryId },
  });

  if (loading) {
    return <p className={styles.text}>Loading...</p>;
  }

  if (error) {
    return (
      <p className={styles.text}>
        An error ocurred while fetching the country data.
      </p>
    );
  }

  const {
    country: {
      name,
      emoji,
      continent: { name: continent },
      languages,
      capital,
      phone,
      currency,
    },
  } = data;
  return (
    <>
      <button onClick={close} className={styles.closeButton} aria-label="Close">
        <CloseIcon />
      </button>
      <h1 className={styles.title}>
        {name} {emoji}
      </h1>
      <p className={styles.text}>
        {name} is a country located in {continent}, they speak{" "}
        {getLanguagesFrom(languages)}, and their capital is {capital}.
      </p>
      <p className={styles.text}>
        If you want to call a phone number in {name}, use the{" "}
        {getPhoneCodesFrom(phone)}, and to check exchange rates for their
        currency, look for {getCurrencyCodesFrom(currency)}.
      </p>
    </>
  );
};

interface ICardProps {
  className: string;
}

const Card = ({ className }: ICardProps): JSX.Element => {
  const [countryId, setCountryId] = useContext(
    CountryContext
  ) as TCountryContext;

  const countryCardProps = {
    close: () => setCountryId(DEFAULT_COUNTRY),
    countryId,
  };

  return (
    <div className={clsx(styles.card, className)}>
      {countryId === DEFAULT_COUNTRY ? (
        <InitialCardContent />
      ) : (
        <CountryCardContent {...countryCardProps} />
      )}
    </div>
  );
};

export default Card;
