import React from "react";

import styles from "./style.module.css";

interface IControlButtonProps {
  ariaLabel: string;
  icon: JSX.Element;
  label?: string;
  onClick: () => void;
}

const ControlButton = ({
  ariaLabel,
  icon,
  label,
  onClick,
}: IControlButtonProps) => {
  const attributes = {
    "aria-label": ariaLabel,
    onClick,
    className: styles.controlButton,
  };
  return (
    <button {...attributes}>
      {icon}
      {label ? <span className={styles.label}>{label}</span> : ""}
    </button>
  );
};

export default ControlButton;
