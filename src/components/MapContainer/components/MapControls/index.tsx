import React from "react";

import RestartAltIcon from "@mui/icons-material/RestartAlt";

import ControlButton from "../ControlButton";

import styles from "./style.module.css";

export interface IMapControlsProps {
  reset: () => void;
}

const MapControls = ({ reset }: IMapControlsProps): JSX.Element => {
  const buttonsProps = {
    reset: {
      ariaLabel: "Reset",
      icon: <RestartAltIcon />,
      label: "Reset",
      onClick: reset,
    },
  };

  return (
    <div className={styles.mapControls}>
      <ControlButton {...buttonsProps.reset} />
    </div>
  );
};

export default MapControls;
