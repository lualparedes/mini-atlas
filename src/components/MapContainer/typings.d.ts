interface ICountries {
  [CountryID: string]: string;
}

interface IMapPath {
  [CountryID: string]: string;
}

interface IMapPaths {
  [CountryID: string]: IMapPath;
}
