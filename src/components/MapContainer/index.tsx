import React, {
  useContext,
  useEffect,
  useRef,
  useState,
  KeyboardEvent,
} from "react";

import clsx from "clsx";
import * as d3 from "d3";

import CountryContext, { TCountryContext } from "context";
import { BLUE_CLEAR } from "style/constants";

import MapControls, { IMapControlsProps } from "./components/MapControls";

import { countries, mapPaths } from "./constants";
import styles from "./style.module.css";

interface IMapContainerProps {
  className: string;
}

const handleEnter =
  (setCountry: (countryId: string) => void, countryId: string) =>
  (event: KeyboardEvent<SVGPathElement>) => {
    if (event.charCode === 13) {
      setCountry(countryId);
    }
  };

const MapContainer = ({ className }: IMapContainerProps): JSX.Element => {
  const gRef = useRef<SVGGElement>(null);
  const [mapControlsProps, setMapControlsProps] = useState<IMapControlsProps>();
  const [country, setCountry] = useContext(CountryContext) as TCountryContext;

  useEffect(() => {
    const svgSelection = d3.select<SVGGElement, unknown>(
      gRef.current as SVGGElement
    );
    const zoom = d3
      .zoom<SVGGElement, unknown>()
      .scaleExtent([1, 20])
      .translateExtent([
        [0, 0],
        [2000, 1000],
      ])
      .on("zoom", (event) => {
        svgSelection.attr("transform", event.transform);
      });

    svgSelection.call(zoom);

    setMapControlsProps({
      reset: () => {
        svgSelection
          .transition()
          .duration(250)
          .call(zoom.transform, d3.zoomIdentity);
      },
    });
  }, []);

  return (
    <div className={clsx(styles.mapContainer, className)}>
      {mapControlsProps && <MapControls {...mapControlsProps} />}
      <svg viewBox="0 0 2000 1000" className={styles.mapFigure}>
        <title>World map</title>
        <pattern
          className={styles.pattern}
          id="diagonalHatch"
          width="4"
          height="4"
          patternTransform="rotate(45 0 0)"
          patternUnits="userSpaceOnUse"
        >
          <line x1="0" y1="0" x2="0" y2="4" />
        </pattern>
        <g className={styles.mapGroup} ref={gRef}>
          <rect x="0" y="0" width="2000" height="1000" fill={BLUE_CLEAR} />
          {Object.entries(mapPaths).map(([countryId, pathData]) => {
            return (
              <path
                onClick={() => setCountry(countryId)}
                onKeyPress={handleEnter(setCountry, countryId)}
                className={clsx(
                  styles.countryMap,
                  country === countryId && styles.selectedCountry
                )}
                d={pathData.d}
                id={`map-${countryId}`}
                key={`map-${countryId}`}
                tabIndex={0}
                aria-labelledby={`title-${countryId}`}
              >
                <title id={`title-${countryId}`}>{countries[countryId]}</title>
              </path>
            );
          })}
        </g>
      </svg>
    </div>
  );
};

export default MapContainer;
