import React, { useState } from "react";

import Header from "components/Header";
import Main from "components/Main";

import CountryContext, { DEFAULT_COUNTRY } from "context";
import "style/global.css";

const App = () => {
  const context = useState(DEFAULT_COUNTRY);
  return (
    <div className="App">
      <CountryContext.Provider value={context}>
        <Header />
        <Main />
      </CountryContext.Provider>
    </div>
  );
};

export default App;
