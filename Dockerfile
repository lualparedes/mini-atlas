FROM node:alpine
RUN mkdir /home/node/app
WORKDIR /home/node/app
COPY . .
RUN npm i
CMD ["npm", "start"]
